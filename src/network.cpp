#include "porterrno.h"
#include "network.h"

#include <WinSock2.h>
#include <WS2tcpip.h>
#include <sstream>

//==================================================================================//
int init_network ()
{
	WSADATA wsa;
	return WSAStartup ( MAKEWORD ( 2, 0 ), &wsa );
}

//==================================================================================//
int connect_to ( const char* address, int port, int protocol )
{
	SOCKET s = INVALID_SOCKET;
	struct addrinfo* result = NULL, hints;

	ZeroMemory ( &hints, sizeof ( hints ) );
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	std::ostringstream oss;
	oss << port;
	int iResult = getaddrinfo ( address, oss.str ().c_str (), &hints, &result );
	if ( iResult != 0 )
		return RES_UNRESOLVABLE_ADDRESS;

	s = socket ( result->ai_family, result->ai_socktype, result->ai_protocol );
	if ( s == INVALID_SOCKET )
		return RES_SOCK_CREATE_FAIL;

	iResult = connect ( s, result->ai_addr, ( int ) result->ai_addrlen );
	closesocket ( s );
	freeaddrinfo ( result );

	return ( iResult == SOCKET_ERROR ? RES_CLOSED_PORT : RES_OPEN_PORT );
}

//==================================================================================//
void shutdown_network ()
{
	WSACleanup ();
}