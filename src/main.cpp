//==================================================================================//
// main.cpp
//
// The main method of the portscan tool.
//==================================================================================//

#include "porterrno.h"
#include "network.h"
#include "protocol.h"
#include "thread.h"

#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>

struct
{
	// The address to connect to:
	const char* address;
	// The minimum and maximum port to scan:
	int minPort, maxPort;

	// The protocol to scan for (TCP / UDP):
	int protocol;

	// The number of threads to use for the port scan:
	int numThreads;
	Hthread* threads;

	// The current port to be tested:
	int currentPort;
	Hmutex currentPortMutex;
} g_CmdArgs;

int parse_cmd_args ( int argc, char* argv[] );
int parse_port_range ( const char* range );
long mem_told ( const char* buf, size_t len );
const char* get_errstr ( int errnum );
void print_help ();

void execute_port_connect ();

int main ( int argc, char* argv[] )
{
	int errnum = -1;
	if ( ( errnum = parse_cmd_args ( argc, argv ) ) )
	{
		if ( errnum != RES_HELP )
			std::printf( "!> Failed to parse command-line arguments: %s\n", get_errstr ( errnum ) );
		else
			print_help ();
		return errnum;
	}

	std::printf ( "Running portscan...\n" );
	std::printf ( "\taddress = %s\n", g_CmdArgs.address );
	std::printf ( "\tminPort = %i\n", g_CmdArgs.minPort );
	std::printf ( "\tmaxPort = %i\n", g_CmdArgs.maxPort );

	std::printf ( "\n" );


	std::printf ( " Creating mutex...\n" );
	if ( create_mutex ( &g_CmdArgs.currentPortMutex ) )
	{
		std::printf ( "!> Failed to create mutex!\n" );
		return RES_FAIL;
	}

	std::printf ( " Initializing network...\n" );
	if ( ( errnum = init_network () ) )
	{
		std::printf ( "!> Failed to initialize network!\n" );
		return errnum;
	}

	std::printf ( " Beginning scan with %i worker thread(s)...\n", g_CmdArgs.numThreads );

	g_CmdArgs.currentPort = g_CmdArgs.minPort;

	// Create the threads:
	g_CmdArgs.threads = new Hthread[ g_CmdArgs.numThreads ];
	for ( int i = 0; i < g_CmdArgs.numThreads; ++i )
	{
		if ( create_thread ( &g_CmdArgs.threads[ i ], execute_port_connect ) != RES_NOERROR )
		{
			std::printf ( " Failed to create worker %i!\n", i );
			shutdown_network ();
			return RES_FAIL;
		}
	}

	std::printf ( "\n ------------------------------------------\n" );

	// Wait for all threads to complete their respective work:
	for ( int i = 0; i < g_CmdArgs.numThreads; ++i )
	{
		join_thread ( &g_CmdArgs.threads[ i ] );
	}

	std::printf ( " ------------------------------------------\n" );

	std::printf ( " Destroying worker threads...\n" );
	for ( int i = 0; i < g_CmdArgs.numThreads; ++i )
	{
		destroy_thread ( &g_CmdArgs.threads[ i ] );
	}

	std::printf ( " Shutting down network...\n" );
	shutdown_network ();

	std::printf ( " Destroying mutex...\n" );
	destroy_mutex ( &g_CmdArgs.currentPortMutex );

	std::printf ( " Done!" );
	return 0;
}


int parse_cmd_args ( int argc, char* argv[] )
{
	// Default values:
	g_CmdArgs.address = NULL;
	g_CmdArgs.minPort = g_CmdArgs.maxPort = -1;
	g_CmdArgs.numThreads = 1;
	g_CmdArgs.protocol = PROTOCOL_TCP;

	int gcmd = 0;
	int errnum = 0;

	for ( int i = 1; i < argc; ++i )
	{
		// Check whether or not we've got a specific argument passed here:
		if ( std::strcmp ( argv[ i ], "-t" ) == 0 )
		{
			g_CmdArgs.numThreads = std::atoi ( argv[ ++i ] );
			continue;
		}
		else if ( std::strcmp ( argv[ i ], "-h" ) == 0 || std::strcmp ( argv[ i ], "--help" ) == 0 )
		{
			return RES_HELP;
		}

		// Parse the unspecified argument:
		switch ( gcmd )
		{
			case 0: 
			{
				g_CmdArgs.address = argv[ i ]; 
				gcmd++;
			} break;
			case 1:
			{
				if ( ( errnum = parse_port_range ( argv[ i ] ) ) != RES_NOERROR )
					return errnum;
				gcmd++;
			} break;
			default: return RES_INVALID_ARGS;
		}
	}

	if ( g_CmdArgs.address == NULL )
	{
		std::printf ( "Missing host-address!\n" );
		return RES_INVALID_ARGS;
	}
	if ( g_CmdArgs.minPort <= -1 || g_CmdArgs.maxPort <= -1 )
	{
		std::printf ( "Missing port range!\n" );
		return RES_INVALID_ARGS;
	}
	if ( g_CmdArgs.numThreads <= 0 )
	{
		std::printf ( "Invalid number of worker threads!\n" );
		return RES_INVALID_ARGS;
	}

	return RES_NOERROR;

}

int parse_port_range ( const char* range )
{
	int maxlen = std::strlen ( range );

	int firstlen = 0, secondlen = 0;
	char c = ' ';

	for ( int i = 0; i < maxlen; ++i )
	{
		c = range[ i ];
		if ( std::isdigit ( c ) )
			firstlen++;
		else
			break;
	}

	if ( c == '-' )
	{
		for ( int i = firstlen + 1; i < maxlen; ++i )
		{
			c = range[ i ];
			if ( std::isdigit ( c ) )
				secondlen++;
			else
				return RES_INVALID_PORTS;
		}

		g_CmdArgs.minPort = mem_told ( range, firstlen );
		g_CmdArgs.maxPort = mem_told ( &range[ firstlen + 1 ], secondlen );
	}
	else
	{
		g_CmdArgs.minPort = g_CmdArgs.maxPort = mem_told ( range, firstlen );
	}

	return RES_NOERROR;
}

long mem_told ( const char* buf, size_t len )
{
	long n = 0, sign = 1;

	while ( len && std::isspace ( *buf ) )
		--len, ++buf;

	if ( len )
	{
		switch ( *buf )
		{
			case '-': sign = -1;
			case '+': --len, ++buf;
		}
	}

	while ( len-- && std::isdigit ( *buf ) )
		n = n * 10 + *buf++ - '0';

	return n * sign;
}

const char* get_errstr ( int errnum )
{
	switch ( errnum )
	{
		case RES_NOERROR: return "noerr";
		case RES_INVALID_ARGS: return "invalid_args";
		case RES_INVALID_PORTS: return "invalid_port_range";
		case RES_UNRESOLVABLE_ADDRESS: return "unresolvable_address";
		case RES_SOCK_CREATE_FAIL: return "sock_create_fail";
		case RES_OPEN_PORT: return "open";
		case RES_CLOSED_PORT: return "closed";
		default: return "unknown_err";
	}
}

void print_help ()
{
	std::printf ( "\n" );
	std::printf ( "Usage: portscan [options] <host> <first[-last]>\n" );
	std::printf ( "Options:\n" );
	std::printf ( "\t-t\t\tSpecify the number of worker threads\n" );
	std::printf ( "\t-h, --help\tShows the help text\n" );
	std::printf ( "\n" );
}

//==================================================================================//
// Asynchronous port connection
//==================================================================================//
void execute_port_connect ()
{
	int port = -1;
	while ( true )
	{
		// Obtain the port number of the next port to connect to:
		lock_mutex ( &g_CmdArgs.currentPortMutex );
		port = g_CmdArgs.currentPort++;
		release_mutex ( &g_CmdArgs.currentPortMutex );

		if ( port > g_CmdArgs.maxPort )
			break; // We're done.

		std::printf ( " %s@%i : status=%s\n", g_CmdArgs.address, port, get_errstr ( connect_to ( g_CmdArgs.address, port, g_CmdArgs.protocol ) ) );
	}
}