#include "porterrno.h"
#include "thread.h"

#include <Windows.h>

//==================================================================================//
DWORD WINAPI global_thread_entry ( LPVOID lpParam )
{
	( ( HthreadEntryProc ) lpParam )( );
	return 0;
}

//==================================================================================//
int create_mutex ( Hmutex* mutex )
{
	mutex->extra = CreateMutex ( NULL, FALSE, NULL );
	return ( mutex->extra == NULL ? RES_FAIL : RES_NOERROR );
}

//==================================================================================//
void lock_mutex ( Hmutex* mutex )
{
	WaitForSingleObject ( mutex->extra, INFINITE );
}

//==================================================================================//
void release_mutex ( Hmutex* mutex )
{
	ReleaseMutex ( mutex->extra );
}

//==================================================================================//
void destroy_mutex ( Hmutex* mutex )
{
	CloseHandle ( mutex->extra );
}

//==================================================================================//
int create_thread ( Hthread* thread, HthreadEntryProc proc )
{
	thread->extra = CreateThread ( NULL, 0, global_thread_entry, proc, 0, &thread->id );
	return ( thread->extra == NULL ? RES_FAIL : RES_NOERROR );
}

//==================================================================================//
void join_thread ( Hthread* thread )
{
	WaitForSingleObject ( thread->extra, INFINITE );
}

//==================================================================================//
void destroy_thread ( Hthread* thread )
{
	CloseHandle ( thread->extra );
}