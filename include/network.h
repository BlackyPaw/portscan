#ifndef __NETWORK_H__
#define __NETWORK_H__

//==================================================================================//
// Name: init_network()
//
// Initializes the whole networking needed for performing the port scan operation(s).
//==================================================================================//
extern int init_network ();

//==================================================================================//
// Name: connect_to()
//
// Connects to the given address at the given port and returns a result code
// indicating the status of the given port.
//
// address - The address to connect to.
// port - The port to connect to.
// protocol - The protocol to use for connecting
//		0 - TCP
//		1 - UDP
//==================================================================================//
extern int connect_to ( const char* address, int port, int protocol );

//==================================================================================//
// Name: shutdown_network()
//
// Shutsdown any resources belonging to the used network.
//==================================================================================//
extern void shutdown_network ();

#endif // __NETWORK_H__