#ifndef __PORT_ERRNO_H__
#define __PORT_ERRNO_H__

#define RES_NOERROR					0
#define RES_INVALID_ARGS			1
#define RES_INVALID_PORTS			2

#define RES_FAIL					3

#define RES_HELP					5

#define RES_UNRESOLVABLE_ADDRESS	10
#define RES_SOCK_CREATE_FAIL		11

#define RES_OPEN_PORT				101
#define RES_CLOSED_PORT				102

#endif // __PORT_ERRNO_H__