#ifndef __THREAD_H__
#define __THREAD_H__

typedef void ( *HthreadEntryProc ) ( );

typedef struct
{
#if defined(_WIN32)
	unsigned long id;
	void* extra;
#endif // _WIN32
} Hthread;

typedef struct
{
#if defined(_WIN32)
	void* extra;
#endif // _WIN32
} Hmutex;


//==================================================================================//
// create_mutex()
//
// Creates a mutex.
//==================================================================================//
extern int create_mutex ( Hmutex* mutex );

//==================================================================================//
// lock_mutex()
//
// Locks the given mutex.
//==================================================================================//
extern void lock_mutex ( Hmutex* mutex );

//==================================================================================//
// release_mutex()
//
// Releases the given mutex.
//==================================================================================//
extern void release_mutex ( Hmutex* mutex );

//==================================================================================//
// destroy_mutex()
//
// Destroys the given mutex.
//==================================================================================//
extern void destroy_mutex ( Hmutex* mutex );

//==================================================================================//
// create_thread()
//
// Creates a thread immediately running after its creation.
//  thread - A pointer to the thread structure to be filled out.
//	proc - The entry procedure of the thread.
//==================================================================================//
extern int create_thread ( Hthread* thread, HthreadEntryProc proc );

//==================================================================================//
// join_thread()
//
// Joins the given thread thereby putting the current one to sleep.
//==================================================================================//
extern void join_thread ( Hthread* thread );

//==================================================================================//
// destroy_thread()
//
// Destroys the given thread.
//==================================================================================//
extern void destroy_thread ( Hthread* thread );

#endif // __THREAD_H__